# Generated by Django 3.2.5 on 2021-07-10 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('musicshop', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='final_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, verbose_name='Общая цена'),
        ),
        migrations.AlterField(
            model_name='cart',
            name='products',
            field=models.ManyToManyField(blank=True, related_name='related_cart', to='musicshop.CartProduct', verbose_name='Продукты для корзины'),
        ),
    ]
